package services

import (
	"reflect"
	"testing"
)

func TestHelloService(t *testing.T) {
	s := NewHelloService()
	result := s.FakeComputation(4)
	if (reflect.TypeOf(result).Kind()) != reflect.String {
		t.Errorf("Expected result to be of string type, got '%T'", result)
	}
}
