package services

import (
	"math/rand"
	"time"
)

type HelloService struct{}

func MockFunction() string {
	return "Hello World"
}

func NewHelloService() *HelloService {
	return &HelloService{}
}

func (s *HelloService) FakeComputation(number int) string {
	rand.Seed(time.Now().UnixNano())
	randomNumber := rand.Int()
	if number == randomNumber {
		return "Hurrah! you got the number right"
	} else if number > randomNumber {
		return "Too high"
	}
	return "Too low"
}
