package controllers

import (
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"
)

func TestHello(t *testing.T) {
	t.Run("Hello", func(t *testing.T) {
		w := httptest.NewRecorder()
		r, err := http.NewRequest("GET", "/hello", nil)
		if err != nil {
			t.Errorf("Failed to create request: %v", err)
		}
		Hello(w, r)
		if w.Code != 200 {
			t.Errorf("Expected status code 200, got %d", w.Code)
		}
		if reflect.TypeOf(w.Body.String()).Kind() != reflect.String {
			t.Errorf("Expected result to be of string type, got '%T'", w.Body.String())
		}
	})
}
