package controllers

import (
	"net/http"

	services "github.com/ardouapex/code-quality-bots/services"
)

func Hello(w http.ResponseWriter, r *http.Request) {
	helloService := services.NewHelloService()
	result := helloService.FakeComputation(5)
	w.Write([]byte(result))
}
