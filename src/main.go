package main

import (
	"fmt"
	"net/http"

	controllers "github.com/ardouapex/code-quality-bots/controllers"
)

func main() {
	http.HandleFunc("/hello", controllers.Hello)
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Println(r.RequestURI)
		w.Write([]byte("Hello, World"))
	})
	fmt.Println("Starting server and listening on port 3001")
	error := http.ListenAndServe(":3001", nil)
	if error != nil {
		panic(error)
	}
}
