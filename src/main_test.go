package main

import (
	"net/http"
	"testing"

	"github.com/ardouapex/code-quality-bots/controllers"
)

func TestMain(t *testing.T) {
	http.HandleFunc("/hello", controllers.Hello)
	srv := &http.Server{Addr: ":8080"}
	go func() {
		srv.ListenAndServe()
	}()
	t.Cleanup(func() {
		srv.Shutdown(nil)
	})
	resp, err := http.Get("http://localhost:8080/hello")
	if err != nil {
		t.Errorf("Failed to get response: %v", err)
	}
	if resp.StatusCode != http.StatusOK {
		t.Errorf("Expected status code 200, got %d", resp.StatusCode)
	}
}
